package controler;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;

public class OperationTest {

	@Test
	public void VerserTest() {
		Operation o = new Operation();
		o.initClientCompte();
		assertEquals("Solde apres operation : 10000.0€", o.verser("Remi", 10000));
	}
}
