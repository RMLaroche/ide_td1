package model;

import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Before;

public class ClientTest {
	
	@Before
	public void setup() {
		//Faire qqchose avant chaque test
	}
	
	@Test
	public void toStringTest() {
		Client clientTest = new Client(1, "LOUBIOU", "remi.loubiou@epsi.fr", 1);
		assertEquals(clientTest.toString(), "Client [code=1, nom=LOUBIOU, email=remi.loubiou@epsi.fr, codeCompte=1]");
	}

}
